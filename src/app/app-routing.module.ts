import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactoComponent } from './components/routes/contacto/contacto.component';
import { GaleriaComponent } from './components/routes/galeria/galeria.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { ServiciosComponent } from './components/routes/servicios/servicios.component';


const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent,
    pathMatch: 'full'
  },
  {
    path: 'galeria',
    component: GaleriaComponent
  },
  {
    path: 'servicios',
    component: ServiciosComponent
  },
  {
    path: 'contacto',
    component: ContactoComponent
  },
  {
    path: '**',
    redirectTo: 'inicio'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
